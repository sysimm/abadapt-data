# Companion data for AbAdapt

* Contents
1. abadapt_holdout.tgz (data files for holdout set)
2. abadapt_train.tgz (data files from training)
3. abadapt_zdock.tgz (data files for ZDOCK benchmark)
4. loocv_full_rmsd.tsv (full-length RMSDs for each Ab and Ag chain, along with the overall sequence identity of each LOOCV antigen model to the structural template)
5. holdout_full_rmsd.tsv (same as above but for holdout models)

* Data files

Each subfolder (named `PDB ID`) in the gzipped archives should contain the following files:

`PDB ID`_nat_ab.pdb (the actual Antibody PDB entry)  
`PDB ID`_nat_ag.pdb (the actual Antigen PDB entry)  
`PDB ID`_nat_complex.pdb  (the actual Antibody-Antigen complex PDB entry)  
`PDB ID`_nat_paratope.txt (antibody residues within 5A of the antigen)  
`PDB ID`_nat_epitope.txt  (antigen residues within 5A of the antibody)  
`PDB ID`_mod_ab.pdb (Repertoire Builder antibody model used for docking)  
`PDB ID`_mod_ag.pdb (antigen model renumbered to single chain 'A')  
`PDB ID`_ref_complex.pdb (combined antibody and antigen model superimposed on native paratope/epitope)
`PDB ID`_ref_complex-rmsd (output of the above superposition)
`PDB ID`_ref_paratope.txt (native paratope mapped onto model antibody)  
`PDB ID`_ref_epitope.txt  (native epitope mapped onto model antigen)  
